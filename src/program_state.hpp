#ifndef UNICLI_PROGRAM_STATE_HPP
#define UNICLI_PROGRAM_STATE_HPP

#include "constants.hpp"
#include <string>

namespace unicli {

struct program_state {
    // Flag to signal program termination.
    bool done;
    // Flag for the current operation mode.
    process_mode mode;
    // Flag for the current input mode.
    input_mode input;
    // Text to print before the user input.
    std::string prompt;
    // Maximum number of columns to print before truncating the output and
    // making a new table for comparison.
    int maxcols;
    // Flag that controls whether to automatically adjust the maximum number of
    // columns to the terminals width during startup.
    bool auto_adapt;
    // Unicode character properties to display in info and compare modes.
    int char_properties;
    // Flag that controls whether property names are written out in long or
    // compact abbreviated form.
    bool short_properties;

    program_state() :
        done(false),
        mode(process_mode::MODE_INFO),
        input(input_mode::INPUT_LITERAL),
        prompt("L> "),
        maxcols(10),
        auto_adapt(true),
        char_properties(1 << (int)char_mode::GROUP_BASIC),
        short_properties(true)
    {}
};

program_state load_config(const char* path);
void save_config(program_state& ps);

void print_config_usage();
void handle_get_config(const std::string& param, const program_state& state);
void handle_set_config(const std::string& param, const std::string& value,
    program_state& state);

} // namespace unicli

#endif

