#include "constants.hpp"

namespace unicli {

std::array<const char*, 64> short_properties = {{
    // Unicode properties, aligned with UCHAR_BINARY_START/UCHAR_BINARY_LIMIT
    "Alpha",
    "AHex",
    "Bidi_C",
    "Bidi_M",
    "Dash",
    "DI",
    "Dep",
    "Dia",
    "Ext",
    "Comp_Ex",
    "Gr_Base",
    "Gr_Ext",
    "Gr_Link",
    "Hex",
    "Hyphen",
    "IDC",
    "IDS",
    "Ideo",
    "IDSB",
    "IDST",
    "Join_C",
    "LOE",
    "Lower",
    "Math",
    "NChar",
    "QMark",
    "Radical",
    "SD",
    "Term",
    "UIdeo",
    "Upper",
    "WSpace",
    "XIDC",
    "XIDS",
    "Sensitive",
    "STerm",
    "VS",
    "nfdinert",
    "nfkdinert",
    "nfcinert",
    "nfkcinert",
    "segstart",
    "Pat_Syn",
    "Pat_WS",
    "alnum",
    "blank",
    "graph",
    "print",
    "xdigit",
    "Cased",
    "CI",
    "CWL",
    "CWU",
    "CWT",
    "CWCF",
    "CWCM",
    "CWKCF",
    // Basic properties
    "Name",
    "Glyph",
    "Code point",
    "UTF-16",
    "UTF-8",
    "Block",
    ""
}};

std::array<const char*, 64> long_properties = { {
    // Unicode properties, aligned with UCHAR_BINARY_START/UCHAR_BINARY_LIMIT
    "Alphabetic",
    "ASCII_Hex_Digit",
    "Bidi_Control",
    "Bidi_Mirrored",
    "Dash",
    "Default_Ignorable_Code_Point",
    "Deprecated",
    "Diacritic",
    "Extender",
    "Full_Composition_Exclusion",
    "Grapheme_Base",
    "Grapheme_Extend",
    "Grapheme_Link",
    "Hex_Digit",
    "Hyphen",
    "ID_Continue",
    "ID_Start",
    "Ideographic",
    "IDS_Binary_Operator",
    "IDS_Trinary_Operator",
    "Join_Control",
    "Logical_Order_Exception",
    "Lowercase",
    "Math",
    "Noncharacter_Code_Point",
    "Quotation_Mark",
    "Radical",
    "Soft_Dotted",
    "Terminal_Punctuation",
    "Unified_Ideograph",
    "Uppercase",
    "White_Space",
    "XID_Continue",
    "XID_Start",
    "Case_Sensitive",
    "STerm",
    "Variation_Selector",
    "NFD_Inert",
    "NFKD_Inert",
    "NFC_Inert",
    "NFKC_Inert",
    "Segment_Starter",
    "Pattern_Syntax",
    "Pattern_White_Space",
    "alnum",
    "blank",
    "graph",
    "print",
    "xdigit",
    "Cased",
    "Case_Ignorable",
    "Changes_When_Lowercased",
    "Changes_When_Uppercased",
    "Changes_When_Titlecased",
    "Changes_When_Casefolded",
    "Changes_When_Casemapped",
    "Changes_When_NFKC_Casefolded",
    // Basic properties
    "Name",
    "Glyph",
    "Code point",
    "UTF-16",
    "UTF-8",
    "Block",
    ""
}};

std::array<const char*, 221> block_names = {{
    "ASCII",
    "Aegean_Numbers",
    "Alchemical",
    "Alphabetic_PF",
    "Ancient_Greek_Music",
    "Ancient_Greek_Numbers",
    "Ancient_Symbols",
    "Arabic",
    "Arabic_Ext_A",
    "Arabic_Math",
    "Arabic_PF_A",
    "Arabic_PF_B",
    "Arabic_Sup",
    "Armenian",
    "Arrows",
    "Avestan",
    "Balinese",
    "Bamum",
    "Bamum_Sup",
    "Batak",
    "Bengali",
    "Block_Elements",
    "Bopomofo",
    "Bopomofo_Ext",
    "Box_Drawing",
    "Brahmi",
    "Braille",
    "Buginese",
    "Buhid",
    "Byzantine_Music",
    "CJK",
    "CJK_Compat",
    "CJK_Compat_Forms",
    "CJK_Compat_Ideographs",
    "CJK_Compat_Ideographs_Sup",
    "CJK_Ext_A",
    "CJK_Ext_B",
    "CJK_Ext_C",
    "CJK_Ext_D",
    "CJK_Radicals_Sup",
    "CJK_Strokes",
    "CJK_Symbols",
    "Carian",
    "Chakma",
    "Cham",
    "Cherokee",
    "Compat_Jamo",
    "Control_Pictures",
    "Coptic",
    "Counting_Rod",
    "Cuneiform",
    "Cuneiform_Numbers",
    "Currency_Symbols",
    "Cypriot_Syllabary",
    "Cyrillic",
    "Cyrillic_Ext_A",
    "Cyrillic_Ext_B",
    "Cyrillic_Sup",
    "Deseret",
    "Devanagari",
    "Devanagari_Ext",
    "Diacriticals",
    "Diacriticals_For_Symbols",
    "Diacriticals_Sup",
    "Dingbats",
    "Domino",
    "Egyptian_Hieroglyphs",
    "Emoticons",
    "Enclosed_Alphanum",
    "Enclosed_Alphanum_Sup",
    "Enclosed_CJK",
    "Enclosed_Ideographic_Sup",
    "Ethiopic",
    "Ethiopic_Ext",
    "Ethiopic_Ext_A",
    "Ethiopic_Sup",
    "Geometric_Shapes",
    "Georgian",
    "Georgian_Sup",
    "Glagolitic",
    "Gothic",
    "Greek",
    "Greek_Ext",
    "Gujarati",
    "Gurmukhi",
    "Half_And_Full_Forms",
    "Half_Marks",
    "Hangul",
    "Hanunoo",
    "Hebrew",
    "High_PU_Surrogates",
    "High_Surrogates",
    "Hiragana",
    "IDC",
    "IPA_Ext",
    "Imperial_Aramaic",
    "Indic_Number_Forms",
    "Inscriptional_Pahlavi",
    "Inscriptional_Parthian",
    "Jamo",
    "Jamo_Ext_A",
    "Jamo_Ext_B",
    "Javanese",
    "Kaithi",
    "Kana_Sup",
    "Kanbun",
    "Kangxi",
    "Kannada",
    "Katakana",
    "Katakana_Ext",
    "Kayah_Li",
    "Kharoshthi",
    "Khmer",
    "Khmer_Symbols",
    "Lao",
    "Latin_1",
    "Latin_Ext_A",
    "Latin_Ext_Additional",
    "Latin_Ext_B",
    "Latin_Ext_C",
    "Latin_Ext_D",
    "Lepcha",
    "Letterlike_Symbols",
    "Limbu",
    "Linear_B_Ideograms",
    "Linear_B_Syllabary",
    "Lisu",
    "Low_Surrogates",
    "Lycian",
    "Lydian",
    "Mahjong",
    "Malayalam",
    "Mandaic",
    "Math_Alphanum",
    "Math_Operators",
    "Meetei_Mayek",
    "Meetei_Mayek_Ext",
    "Meroitic_Cursive",
    "Meroitic_Hieroglyphs",
    "Miao",
    "Misc_Arrows",
    "Misc_Math_Symbols_A",
    "Misc_Math_Symbols_B",
    "Misc_Pictographs",
    "Misc_Symbols",
    "Misc_Technical",
    "Modifier_Letters",
    "Modifier_Tone_Letters",
    "Mongolian",
    "Music",
    "Myanmar",
    "Myanmar_Ext_A",
    "NB",
    "NKo",
    "New_Tai_Lue",
    "Number_Forms",
    "OCR",
    "Ogham",
    "Ol_Chiki",
    "Old_Italic",
    "Old_Persian",
    "Old_South_Arabian",
    "Old_Turkic",
    "Oriya",
    "Osmanya",
    "PUA",
    "Phags_Pa",
    "Phaistos",
    "Phoenician",
    "Phonetic_Ext",
    "Phonetic_Ext_Sup",
    "Playing_Cards",
    "Punctuation",
    "Rejang",
    "Rumi",
    "Runic",
    "Samaritan",
    "Saurashtra",
    "Sharada",
    "Shavian",
    "Sinhala",
    "Small_Forms",
    "Sora_Sompeng",
    "Specials",
    "Sundanese",
    "Sundanese_Sup",
    "Sup_Arrows_A",
    "Sup_Arrows_B",
    "Sup_Math_Operators",
    "Sup_PUA_A",
    "Sup_PUA_B",
    "Sup_Punctuation",
    "Super_And_Sub",
    "Syloti_Nagri",
    "Syriac",
    "Tagalog",
    "Tagbanwa",
    "Tags",
    "Tai_Le",
    "Tai_Tham",
    "Tai_Viet",
    "Tai_Xuan_Jing",
    "Takri",
    "Tamil",
    "Telugu",
    "Thaana",
    "Thai",
    "Tibetan",
    "Tifinagh",
    "Transport_And_Map",
    "UCAS",
    "UCAS_Ext",
    "Ugaritic",
    "VS",
    "VS_Sup",
    "Vai",
    "Vedic_Ext",
    "Vertical_Forms",
    "Yi_Radicals",
    "Yi_Syllables",
    "Yijing"
}};

} // unicli

