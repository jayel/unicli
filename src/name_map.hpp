#ifndef UNICLI_NAME_MAP_HPP
#define UNICLI_NAME_MAP_HPP

#include "util.hpp"
#include <unordered_map>
#include <set>
#include <vector>

namespace unicli {

// Maps Unicode character name property to code points
// It maps identifiers from Unicode character names to characters that contain
// that identifier in their name.
typedef std::unordered_map<icu::UnicodeString, std::set<UChar32>> name_map;

name_map create_name_map();
std::vector<icu::UnicodeString> tokenize_name(const icu::UnicodeString& input);

} // unicli

#endif

