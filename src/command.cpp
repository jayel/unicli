#include "command.hpp"
#include "icu_backend.hpp"
#include "constants.hpp"
#include "input.hpp"
#include "program_state.hpp"
#include "terminal.hpp"
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <iostream>

namespace boa = boost::algorithm;

template <typename T>
static auto make_command(const std::string& s) ->
    unicli::command_t
{
    return unicli::command_t(T(s));
}

namespace unicli {

std::array<const char*, 13> command_name = { {
    "block",
    "compare",
    "config",
    "exit",
    "help",
    "hex",
    "info",
    "literal",
    "normalize",
    "quit",
    "search",
    "tokenize",
    ""
} };

command_t command_t::create(const std::string& cmd, const std::string& arg)
{
    typedef std::function<command_t (const std::string&)> make_command_fn;
    static std::unordered_map<std::string, make_command_fn> command_generator = {
        {"block",       make_command<set_block>},
        {"compare",     make_command<set_compare>},
        {"config",      make_command<set_config>},
        {"exit",        make_command<quit>},
        {"help",        make_command<help>},
        {"hex",         make_command<set_hex>},
        {"info",        make_command<set_info>},
        {"literal",     make_command<set_literal>},
        {"normalize",   make_command<set_normalize>},
        {"quit",        make_command<quit>},
        {"search",      make_command<set_search>},
        {"tokenize",    make_command<set_tokenize>},
        };
    auto generator = command_generator.find(cmd);
    if (generator != command_generator.end())
        return generator->second(arg);
    return process_text(arg);
}

void quit::run(program_state& ps) const noexcept
{
    ps.done = true;
}

void process_text::run(program_state& state) const
{
    icu::UnicodeString data;
    if (state.input == input_mode::INPUT_LITERAL)
        data = icu::UnicodeString(m_data.c_str());
    else if (state.input == input_mode::INPUT_HEX)
        data = unicli::get_from_hex(m_data);
    else if (state.input == input_mode::INPUT_CHAR_SEARCH)
        data = unicli::get_from_name(m_data);
    else if (state.input == input_mode::INPUT_BLOCK)
        data = unicli::get_from_block(icu::UnicodeString::fromUTF8(m_data));

    if (!data.length()) {
        std::cout << "No characters found.\n";
        return;
    }

    if (state.mode == process_mode::MODE_INFO)
        display(properties(data, state.char_properties), state.short_properties);
    else if (state.mode == process_mode::MODE_COMPARE)
        compare(properties(data, state.char_properties), state.maxcols, state.char_properties, state.short_properties);
    else if (state.mode == process_mode::MODE_NORMALIZE)
        display(normalize(data));
    else if (state.mode == process_mode::MODE_DECOMPOSE)
        display(tokenize(data));
}


void set_block::run(program_state& state) const
{
    std::vector<std::string> tokens;
    boa::split(tokens, m_data, boa::is_space(), boost::token_compress_on);
    assert(tokens.size() > 0);
    if (tokens.size() == 1) {
        state.input = input_mode::INPUT_BLOCK;
        state.prompt[0] = PROMPT_BLOCK;
    }
    else if (tokens[1] == "names") {
        std::ostream_iterator<const char*> out(std::cout, ", ");
        std::copy(block_names.begin(), block_names.end() - 1, out);
        std::cout << block_names.back() << "\n";
    }
}

void set_search::run(program_state& state) const
{
    state.input = input_mode::INPUT_CHAR_SEARCH;
    state.prompt[0] = PROMPT_SEARCH;
}

void set_literal::run(program_state& state) const
{
    state.input = input_mode::INPUT_LITERAL;
    state.prompt[0] = PROMPT_LITERAL;
}

void set_hex::run(program_state& state) const
{
    state.input = input_mode::INPUT_HEX;
    state.prompt[0] = PROMPT_HEX;
}

void set_info::run(program_state& state) const
{
    state.mode = process_mode::MODE_INFO;
    state.prompt[1] = PROMPT_INFO;
}

void set_compare::run(program_state& state) const
{
    state.mode = process_mode::MODE_COMPARE;
    state.prompt[1] = PROMPT_COMPARE;
}

void set_normalize::run(program_state& state) const
{
    state.mode = process_mode::MODE_NORMALIZE;
    state.prompt[1] = PROMPT_NORMALIZE;
}

void set_tokenize::run(program_state& state) const
{
    state.mode = process_mode::MODE_DECOMPOSE;
    state.prompt[1] = PROMPT_TOKENIZE;
}

void set_config::run(program_state& state) const
{
    std::vector<std::string> tokens;
    boa::split(tokens, m_data, boa::is_space(), boost::token_compress_on);
    if (tokens.size() > 2 && tokens[1] == "get")
        handle_get_config(tokens[2], state);
    else if (tokens.size() > 3 && tokens[1] == "set")
        handle_set_config(tokens[2], tokens[3], state);
    else
        print_config_usage();
}

void help::run(program_state&) const
{
    std::vector<std::string> tokens;
    boa::split(tokens, m_data, boa::is_space(), boost::token_compress_on);
    if (tokens.size() < 2) {
        std::cout << "Usage: help [command]\nCommands:\n\t";
        std::ostream_iterator<const char*> out(std::cout, "\n\t");
        std::copy(command_name.begin(), command_name.end() - 1, out);
        std::cout << command_name.back() << "\n";
        return;
    }
    static std::unordered_map<std::string, std::string> data = {
        {"block",
            "USAGE\n"
            "\tblock\n\t\t- Changes the input mode to Unicode block names.\n\t\t"
            "- Block names are case insensitive\n\n"
            "\tblock names\n\t\t- Lists all the available block names.\n"
            "EXAMPLES\n"
            "\t> ascii\n\t\t- Get all characters from the ASCII block.\n"
        },
        {"compare",
            "USAGE\n"
            "\tcompare\n\t\t"
            "- Changes the output mode to a table format.\n\t\t"
            "- Enables easier visualization of data from multiple characters.\n"
        },
        {"config",
            "USAGE\n"
            "\tconfig\n\t\t"
            "- Get or set a configuration parameter.\n\t\t"
            "- See `config` for more details.\n"
        },
        {"exit", "Exit this program.\n"},
        {"help", "This message.\n"},
        {"hex",
            "USAGE\n"
            "\thex\n\t\t- Changes the input mode to hexadecimal.\n\t\t"
            "- Hex letters are case insensitive.\n"
            "EXAMPLES\n"
            "\t> 4f 50\n\t\t- Get the characters 'O' and 'P'\n"
            "\t> a1-f1\n\t\t- Get all the characters between the code points a1 and f1\n"
        },
        {"info",
            "USAGE\n"
            "\tinfo\n\t\t- Changes the output mode to sequential lists.\n\t\t"
            "- This is the default output mode when the program is run for the first time\n"
        },
        {"literal",
            "USAGE\n"
            "\tliteral\n\t\t- Changes input mode to literal characters.\n\t\t"
            "- User input is treated as a character from the system's current "
            "code page\n"
            "EXAMPLES\n"
            "\t> \u20ac\n\t\t- User input is a euro currency sign if the code page is UTF-8.\n"
        },
        {"normalize",
            "USAGE\n"
            "\ttnormalize\n\t\t- Changes the output mode to normalization.\n\t\t"
            "- User input will be normalized to all normalization forms.\n"
        },
        {"quit", "Exit this program.\n"},
        {"search",
            "USAGE\n"
            "\tsearch\n\t\t- Change the input mode to character name search.\n\t\t"
            "- User input will be treated as a partial or full character name.\n\t\t"
            "- If more than one character matches the supplied name, all of them will be used.\n\t\t"
            "- Names are case insensitive.\n"
            "EXAMPLES\n"
            "\t> latin small letter f\n\t\t"
            "- Gets all characters that match that name, in this case 11 code points.\n"
        },
        {"tokenize",
            "USAGE\n"
            "\ttokenize\n\t\t- Changes the output mode to tokenization.\n\t\t" 
            "- User input will be tokenized to glyphs and code points.\n\t\t"
            "- Allows for a very compact view of glyphs and code points.\n"
        }
    };
    auto msg = data.find(tokens[1]);
    if (msg == data.end()) {
        std::cout << "Unknown command.\n";
        return;
    }
    std::cout << msg->second;
}

} // unicli

