#include "program_state.hpp"
#include <fstream>
#include <iostream>
#include <sys/ioctl.h>

///////////////////////////////////////////////////////////////////////////////
// Internal free functions
///////////////////////////////////////////////////////////////////////////////

static int32_t get_term_width()
{
    struct winsize max;
    ioctl(0, TIOCGWINSZ , &max);
    return max.ws_col;
}

template <class T>
static auto operator>>(std::istream& input, T& dest) ->
    typename std::enable_if<
        std::is_enum<typename std::remove_reference<T>::type>::value,
        std::istream&
    >::type
{
    typename std::underlying_type<T>::type tmp;
    input >> tmp;
    dest = static_cast<T>(tmp);
    return input;
}

template <class Enum>
static auto as_integer(Enum value) ->
    typename std::underlying_type<Enum>::type
{
    return static_cast<typename std::underlying_type<Enum>::type>(value);
}

static std::ostream& operator<<(std::ostream& out, const unicli::program_state& ps)
{
    out << as_integer(ps.mode) << '\n'
        << as_integer(ps.input) << '\n'
        << ps.prompt << '\n'
        << ps.maxcols << '\n'
        << ps.auto_adapt << '\n'
        << ps.char_properties << '\n'
        << ps.short_properties << '\n';
    return out;
}

static std::istream& operator>>(std::istream& in, unicli::program_state& ps)
{
    in  >> ps.mode
        >> ps.input
        >> ps.prompt
        >> ps.maxcols
        >> ps.auto_adapt
        >> ps.char_properties
        >> ps.short_properties;
    return in;
}

///////////////////////////////////////////////////////////////////////////////
// Implementation
///////////////////////////////////////////////////////////////////////////////

namespace unicli {

program_state load_config(const char* path)
{
    program_state ps;
    std::ifstream in(path);
    if (in.is_open())
        in >> ps;
    if (ps.auto_adapt)
        ps.maxcols = get_term_width();
    return ps;
}

void save_config(program_state& ps)
{
    std::ofstream out(CONFIG_FILE);
    out << ps;
}

void print_config_usage()
{
    std::cout <<
        "Usage: config [set|get] <parameter> <value>?\n"
        "Parameters:\n\t"
        "maxcols  [n]: terminal width\n\t"
        "basic  [0|1]: basic Unicode properties\n\t"
        "binary [0|1]: binary Unicode properties\n\t"
        "short  [0|1]: print short property names\n";
}

void handle_get_config(const std::string& param, const program_state& state)
{
    if (param == "maxcols")
        std::cout << state.maxcols << '\n';
    else if (param == "basic")
        std::cout << (bool)GET_PROP(GROUP_BASIC, state.char_properties) << '\n';
    else if (param == "binary")
        std::cout << (bool)GET_PROP(GROUP_BINARY, state.char_properties) << '\n';
    else if (param == "short")
        std::cout << state.short_properties << '\n';
    else
        std::cout << "Unknown parameter\n";
}

void handle_set_config(const std::string& param, const std::string& value,
    program_state& state)
{
    if (param == "maxcols")
        state.maxcols = atoi(value.c_str());
    else if (param == "short")
        state.short_properties = atoi(value.c_str());
    else {
        int property = 0;
        bool enabled = atoi(value.c_str());
        if (param == "basic")
            property = GROUP_BASIC;
        else if (param == "binary")
            property = GROUP_BINARY;
        else {
            std::cout << "Unknown parameter\n";
            return;
        }
        if (enabled)
            SET_PROP(property, state.char_properties);
        else
            UNSET_PROP(property, state.char_properties);
        if (!state.char_properties)
            std::cout << "Warning: no character properties group enabled, some output modes won't display anything\n";
    }
}

} // unicli

