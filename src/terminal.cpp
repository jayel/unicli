#include "terminal.hpp"
#include "icu_backend.hpp"
#include "constants.hpp"
#include "util.hpp"
#include <algorithm>
#include <iomanip>
#include <iostream>
#include <unicode/uchar.h>


// Separate function due to all the shenanigans needed to make sure it prints
// properly in a table.
static inline void print_aligned_glyph(const std::string& s, int indent, int col)
{
    UChar32 cp = 0;
    U8_GET_UNSAFE(s.data(), 0, cp);
    if (!u_isprint(cp) || u_isblank(cp)) {
        std::cout << std::setw(indent) << ' ';
        return;
    }
    if (!U8_IS_SINGLE(static_cast<uint8_t>(s[0])))
        indent += U8_COUNT_TRAIL_BYTES(static_cast<uint8_t>(s[0]));
    if (u_hasBinaryProperty(cp, UCHAR_GRAPHEME_EXTEND) && col)
        ++indent;
    std::cout << std::setw(indent) << s;
}

namespace unicli {

void display(const std::vector<property_t>& data, bool short_names)
{
    auto indent = std::setw(short_names ? ALIGN_SHORT : ALIGN_LONG);
    for (const auto& i : data) {
        if (!i.is_separator())
            std::cout << indent << i.name(short_names) << ": " << i.value() << '\n';
        else
            std::cout << '\n';
    }
}

void display(const std::vector<token_t>& tokens)
{
    for (const auto& t : tokens)
        std::cout << '\'' << t.first << "' (" << t.second << "), ";
    std::cout << '\n';
}

void display(const std::vector<normalization_t>& forms)
{
    auto i1 = std::setw(8);
    auto i2 = std::setw(14);
    for (const auto& f : forms)
        std::cout << i1 << f.m_type << i2 << f.m_mode << ": " << f.m_value << '\n';
}

void compare(std::vector<property_t>&& data, int maxcols, int flags,
    bool short_names)
{
    // Remove name property because it's too big and limits the number of columns
    // we can output too much.
    const auto end = std::remove_if(data.begin(), data.end(),
        [](const property_t& t) {
            return t.type() == PROP_BASIC_NAME || t.is_separator();
        });
    // Get the maximum column length so we can properly indent the table.
    const int colwidth = std::max_element(data.begin(), end,
        [](const property_t& l, const property_t& r) {
            return l.value().length() < r.value().length();
        })->value().length() + 2;
    // Calculate the cycle when the properties for a new character begin.
    // Subtract one for the previously removed name property.
    const int cycle =
        (GET_PROP(GROUP_BASIC, flags) ? (PROP_BASIC_END - PROP_BASIC_START) - 1 : 0) +
        (GET_PROP(GROUP_BINARY, flags) ? PROP_UNICODE_END : 0);
    const int chars = std::distance(data.begin(), end) / cycle;
    const int columns =
        ((maxcols - 2 - (short_names ? ALIGN_SHORT : ALIGN_LONG)) /
        colwidth);
    const int rows = (chars / columns) + 1;
    const auto i1 = std::setw(short_names ? ALIGN_SHORT : ALIGN_LONG);
    const auto i2 = std::setw(colwidth);
    // Actually print the characters and the information in a table.
    for (int i = 0; i < rows; ++i) {
        const int base = (i * columns * cycle);
        if (base >= std::distance(data.begin(), end))
            break;
        for (int p = 0; p < cycle; ++p) {
            std::cout << i1 << data[p].name(short_names) << ": ";
            const int offset = base + p;
            for (int j = 0; j < columns; ++j) {
                const int index = offset + (j * cycle);
                if (index >= std::distance(data.begin(), end))
                    break;
                if (data[index].type() == PROP_BASIC_GLYPH)
                    print_aligned_glyph(data[index].value(), colwidth, j);
                else
                    std::cout << i2 << data[index].value();
            }
            std::cout << '\n';
        }
        std::cout << '\n';
    }

}

} // unicli

