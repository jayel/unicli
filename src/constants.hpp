#ifndef UNICLI_CONSTANTS_HPP
#define UNICLI_CONSTANTS_HPP

#include <array>
#include <unicode/uchar.h>

#define CONFIG_FILE ".unicli.cfg"
#define MAX_PROPERTIES 40
#define TABLE_HEADER_LEN 15
#define TABLE_COLUMN_LEN 7

#define UNSET_PROP(property, configset) (configset &= ~property)
#define SET_PROP(property, configset) (configset |= property)
#define GET_PROP(property, configset) (configset & property)

#define PROMPT_BLOCK '#'
#define PROMPT_SEARCH '?'
#define PROMPT_LITERAL 'L'
#define PROMPT_HEX 'X'
#define PROMPT_INFO '>'
#define PROMPT_COMPARE '%'
#define PROMPT_NORMALIZE '='
#define PROMPT_TOKENIZE '~'

#define ALIGN_LONG 29
#define ALIGN_SHORT 15

// Available character properties. Binary Unicode properties are wrapped in this
// enum but their values are conserved as is. See unicode/uchar.h for more 
// information.
// Note that character property groups are defined as:
//      [PROP_<NAME>_START, PROP_<NAME>_END)
// ie: the '_END' constants are one value past the last item in the group.
enum property_types {
    PROP_UNICODE_START = UCHAR_BINARY_START,
    PROP_UNICODE_END = UCHAR_BINARY_LIMIT,
    PROP_BASIC_START = UCHAR_BINARY_LIMIT,
    PROP_BASIC_NAME = PROP_BASIC_START,
    PROP_BASIC_GLYPH,
    PROP_BASIC_UTF32,
    PROP_BASIC_UTF16,
    PROP_BASIC_UTF8,
    PROP_BASIC_BLOCK,
    PROP_BASIC_END,
    PROP_SEPARATOR = PROP_BASIC_END,
};

namespace unicli {

// Enumeration with all available input modes.
// An input mode indicates how user input is interpreted from the command line.
enum class input_mode {
    // Default input mode. Reads user input as a literal string in the system's
    // currently active code page. In most Linux (all?) systems, the default is
    // UTF-8.
    INPUT_LITERAL = 0,
    // Hexadecimal input mode. This allows to:
    // a) Introduce code points in hexadecimal.
    // b) Introduce code point ranges in hexadecimal.
    INPUT_HEX,
    // Character search input mode. The input will be treated as a character name.
    // Note that many characters can match a name, for example "LATIN".
    // If the name supplied has more than one word, it will return the characters
    // that contain all of the strings.
    INPUT_CHAR_SEARCH,
    // Block input mode. The input will be treated as a Unicode block name.
    INPUT_BLOCK,
};

// Enumeration with all available text processing output modes.
enum class process_mode {
    // Default output mode. Lists character properties as a list.
    MODE_INFO = 0,
    // Prints character properties as a table.
    MODE_COMPARE,
    // Normalizes the input string to all normalization forms and prints them.
    MODE_NORMALIZE,
    // Tokenizes the input string to glyphs and code points.
    MODE_DECOMPOSE,
};

// Character property groups. This allows to select in broad strokes which
// character properties to get from the `properties' function.
enum char_mode {
    // General properties, which include name, code point, glyph, etc.
    GROUP_BASIC = 1,
    // Binary Unicode properties. See the Unicode specification for details on
    // what they are and mean.
    GROUP_BINARY = 2,
};

// C-style strings with the shortened names of all character properties,
// including basic and binary properties.
// The strings are valid throughout the program's execution and must not be
// deallocated.
extern std::array<const char*, 64> short_properties;

// C-style strings with the shortened names of all character properties,
// including basic and binary properties.
// The strings are valid throughout the program's execution and must not be
// deallocated.
extern std::array<const char*, 64> long_properties;

// C-style strings with the names of all Unicode blocks.
// including basic and binary properties.
// The strings are valid throughout the program's execution and must not be
// deallocated.
extern std::array<const char*, 221> block_names;

} // namespace unicli

#endif

