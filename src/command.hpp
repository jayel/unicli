#ifndef UNICLI_COMMAND_HPP
#define UNICLI_COMMAND_HPP

#include <memory>
#include <string>
#include <unordered_map>

namespace unicli {

struct program_state;

////////////////////////////////////////////////////////////////////////////////
// Generic command interface.
////////////////////////////////////////////////////////////////////////////////

// Commands that implement functionality should implement a `run` member
// function.
class command_t {
    struct concept_t {
        virtual ~concept_t() = default;
        virtual void run(program_state&) const = 0;
    };

    template <typename T>
    struct model_t : concept_t {
        T m_data;

        model_t(T cmd) noexcept :
            m_data(std::move(cmd))
        {
        }

        void run(program_state& ps) const
        {
            m_data.run(ps);
        }
    };

    std::unique_ptr<const concept_t> m_self;

public:
    template <typename T>
    command_t(T cmd) :
        m_self(new model_t<T>(std::move(cmd)))
    {
    }

    void run(program_state& ps) const
    {
        m_self->run(ps);
    }

    // Creates a command specified by cmd.
    // If cmd is a keyword, it returns one of 'quit', 'config', etc.
    // If not, then the command created is a process_text command.
    static command_t create(const std::string& cmd, const std::string& arg);
};

////////////////////////////////////////////////////////////////////////////////
// Program commands
////////////////////////////////////////////////////////////////////////////////

// Sets the program termination flag to true which causes the program to exit.
struct quit {
    quit(const std::string&) noexcept {}
    void run(program_state&) const noexcept;
};

// Process text input according to the current configuration.
// Depending on the configuration it will interpret the input as:
//  a) an hexadecimal code point or a range of hexadecimal code points
//  b) a literal string
//  c) part of the name of a set Unicode characters
//  d) a Unicode block name
// Then, depending on the selected mode, it will:
//  a) display a list of information about those characters
//  b) display a table of information about those characters
//  c) normalize text
class process_text
{
    std::string m_data;

public:
    process_text(const std::string& s) noexcept : m_data(s) {}
    void run(program_state&) const;
};

// Activates block input mode OR
// Prints all available Unicode block names.
class set_block
{
    std::string m_data;

public:
    set_block(const std::string& s) noexcept : m_data(s) {}

    void run(program_state&) const;
};

// Activates search input mode.
struct set_search
{
    set_search(const std::string&) noexcept {}

    void run(program_state&) const;
};

// Activates literal input mode.
struct set_literal
{
    set_literal(const std::string&) noexcept {}

    void run(program_state&) const;
};

// Activates hexadecimal range and code point input.
struct set_hex
{
    set_hex(const std::string&) noexcept {}

    void run(program_state&) const;
};

// Activates information output mode.
struct set_info
{
    set_info(const std::string&) noexcept {}

    void run(program_state&) const;
};

// Activates compare output mode.
struct set_compare
{
    set_compare(const std::string&) noexcept {}

    void run(program_state&) const;
};

// Activates normalization output mode.
struct set_normalize
{
    set_normalize(const std::string&) noexcept {}

    void run(program_state&) const;
};

// Activates tokenization output mode.
struct set_tokenize
{
    set_tokenize(const std::string&) noexcept {}
    void run(program_state&) const;
};

// Gets a configuration parameter OR
// Sets a configuration parameter OR
// Prints a usage message.
class set_config
{
    std::string m_data;

public:
    set_config(const std::string& s) noexcept : m_data(s) {}
    void run(program_state&) const;
};

// Prints a help message about a command OR
// Prints a usage message.
class help
{
    std::string m_data;

public:
    help(const std::string& s) noexcept : m_data(s) {}
    void run(program_state&) const;

};

// C-style strings with the names of all available commands.
// The names only contain the command name and exclude any kind of possible
// arguments. The strings are valid throughout the program's execution and must
// not be deallocated.
extern std::array<const char*, 13> command_name;

} // unicli

#endif

