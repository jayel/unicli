#ifndef UNICLI_INPUT_HPP
#define UNICLI_INPUT_HPP

#include <unicode/unistr.h>

namespace unicli {

// Returns a string containing all the code points that contain the name 's'.
icu::UnicodeString get_from_name(const std::string& s);

// Returns a string containing all the code points from block 's'.
icu::UnicodeString get_from_block(const icu::UnicodeString& s);

icu::UnicodeString get_from_hex(const std::string& s);

} // unicli

#endif

