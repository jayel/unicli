#ifndef UNICLI_UNICODE_HELPER_HPP
#define UNICLI_UNICODE_HELPER_HPP

#include <string>
#include <unicode/unistr.h>
#include <vector>

namespace unicli {

class property_t;
class normalization_t;
typedef std::pair<std::string, std::string> token_t;

// Display character properties one character after another.
// This function expects there to be separator properties that indicate when to
// start the output for a new character.
//
// `short_names' controls whether to print the abbreviated version of a
// property's name or the full version. For example "LOE" vs
// "Logical_Order_Exception".
void display(const std::vector<property_t>&, bool short_names = true);

// Display character properties in a table. This format is better if one wants
// to compare characters since the information is put in columns which makes
// comparing data easier than scrolling around.
//
// This function expects there to be separator properties that indicate when to
// start the output for a new character.
//
// Note that this function modifies the vector with the properties in place
// to be able to format the output properly. Currently it "removes" (moves
// to the end of the vector) all properties of type name and separator. The
// name property is removed because in general names are very long, which
// severely limits the usability of a table. This will probably be configurable
// in the future.
//
// `macols' indicates the width (number of columns) of the terminal. This ensures
// that the table stays properly formatted.
//
// `short_names' controls whether to print the abbreviated version of a
// property's name or the full version. For example "LOE" vs
// "Logical_Order_Exception".
void compare(std::vector<property_t>&&, int maxcols, int flags,
    bool short_names = true);

// Display token data one after another, separated by commas.
void display(const std::vector<token_t>&);

// Display normalized strings.
void display(const std::vector<normalization_t>& forms);


} // unicli


#endif

