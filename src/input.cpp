#include "input.hpp"
#include "name_map.hpp"
#include <algorithm>
#include <cstdlib>
#include <unicode/normalizer2.h>
#include <unicode/schriter.h>
#include <unicode/uniset.h>
#include <unicode/usetiter.h>

static UChar32 parse_hex(const icu::UnicodeString& input)
{
    UErrorCode err = U_ZERO_ERROR;
    auto normalizer = icu::Normalizer2::getNFKCCasefoldInstance(err);
    icu::UnicodeString tmp;
    if (U_FAILURE(err))
        return 0;
    normalizer->normalize(input, tmp, err);
    std::string dest;
    tmp.toUTF8String(dest);
    return strtoul(dest.c_str(), nullptr, 16);
}

enum class parse_range {
    NOTHING,
    LEFT,
    RIGHT,
    DONE,
};

namespace unicli {

icu::UnicodeString get_from_name(const std::string& s)
{
    static unicli::name_map unidata = create_name_map();
    icu::UnicodeString name_property = icu::UnicodeString::fromUTF8(s);
    name_property.toUpper();
    auto tokens = unicli::tokenize_name(name_property);
    auto found_cp = unidata[tokens.front()];
    std::for_each(tokens.begin() + 1, tokens.end(),
        [&](decltype(tokens.front()) t) {
            const auto& items = unidata[t];
            for (const auto c : found_cp)
                if (!items.count(c))
                    found_cp.erase(c);
        });

    icu::UnicodeString result;
    for (const auto i : found_cp)
        result += i;
    return result;
}

icu::UnicodeString get_from_block(const icu::UnicodeString& s)
{
    UErrorCode ec = U_ZERO_ERROR;
    icu::UnicodeString block("[:Block = ");
    icu::UnicodeString result;
    block.append(s);
    block.append(":]");
    icu::UnicodeSet cps(block, ec);
    if (U_FAILURE(ec))
        return result;
    icu::UnicodeSetIterator i(cps);
    for (; i.next(); )
        result += i.getString();
    return result;
}

icu::UnicodeString get_from_hex(const std::string& data)
{
    UChar32 num = 0;
    UChar32 c = 0;
    icu::UnicodeString result, tmp;
    auto input = icu::UnicodeString::fromUTF8(data);
    icu::StringCharacterIterator i(input);
    parse_range state = parse_range::NOTHING;

    auto add_chars = [&]() {
        if (tmp.length()) {
            if (state == parse_range::LEFT) {
                icu::UnicodeSet cpset(num, parse_hex(tmp));
                icu::UnicodeSetIterator j(cpset);
                for (; j.next(); )
                    result += j.getString();
            }
            else
                result += parse_hex(tmp);
            tmp.remove();
        }
    };


    for (; i.hasNext(); ) {
        c = i.next32PostInc();
        if (u_isWhitespace(c)) {
            add_chars();
            continue;
        }
        if (c == '-' && state == parse_range::NOTHING) {
            state = parse_range::LEFT;
            num = parse_hex(tmp);
            tmp.remove();
        }
        else if (u_isxdigit(c)) {
            tmp += c;
        }
    }
    add_chars();
    return result;
}

} // unicli

