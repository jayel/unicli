#ifndef UNICLI_BACKEND_HPP
#define UNICLI_BACKEND_HPP

#include <string>
#include <unicode/unistr.h>
#include <vector>

namespace unicli {

// Class that represents a character property and a value associated with it.
// Character properties are split in 3 groups: "basic", binary, and a special
// `separator' property.
//
// Basic properties are more general properties, like a character's code point
// in various encodings, the glyph, character name and so on.
//
// Binary (Unicode) properties are more specific properties, for example if a
// character is lowercase and so on. Read the Unicode specification for more
// details on this.
//
// The `separator' property is a dummy type that does nothing other than indicate
// the start of character properties for a new character.
// Used purely to format output.
class property_t {
    int m_type;
    std::string m_value;

public:
    property_t() noexcept : m_type(0) {}
    property_t(int t, int value) noexcept;
    property_t(int t, std::string&& s) noexcept :
        m_type(t),
        m_value(std::move(s))
    {}

    // Returns the numeric value of this type.
    // Properties are mapped as follows:
    // a) From 0 to UCHAR_BINARY_LIMIT: binary Unicode properties.
    // b) From UCHAR_BINARY_LIMIT to UCHAR_BINARY_LIMIT + 6: basic properties.
    // c) UCHAR_BINARY_LIMIT + 6: separator
    int type() const noexcept { return m_type; }

    // Returns a C-style string with the name of this property.
    // The returned string is valid throughout the program's execution and must
    // not be deallocated.
    const char* name(bool short_format = true) const noexcept;

    // Returns the value of this property.
    const std::string& value() const noexcept { return m_value; }

    // Returns true if an instance is a separator.
    bool is_separator() const noexcept;
};

// Class that contains a normalized string and the configuration (the
// normalization type and mode).
// The normalized string should be encoded in UTF-8.
// For more information on normalization see the Unicode specification.
struct normalization_t {
    const char* m_type;
    const char* m_mode;
    std::string m_value;

    normalization_t(const char* t, const char* m, std::string&& v) noexcept :
        m_type(t),
        m_mode(m),
        m_value(std::move(v))
    {}
};

// Type used to store tokens:
// a) The first value is the glyph encoded in UTF-8.
// b) The second value is a string with an hexadecimal code point.
typedef std::pair<std::string, std::string> token_t;

// Processes the input string a returns a vector containing the properties for
// each individual code point found in the string.
// `flags' controls which properties to get. The available property groups are
// found in the `char_mode' enum.
std::vector<property_t> properties(const icu::UnicodeString& s, int flags);

// Normalizes the string to all possible normalization combinations.
// All normalized strings are encoded in UTF-8.
std::vector<normalization_t> normalize(const icu::UnicodeString& s);

// Tokenizes the input string to glyphs and code points.
// Similar to `properties' but stripped down. Because it only returns two
// properties, it's much faster and the output can be more compact.
std::vector<token_t> tokenize(const icu::UnicodeString& input);

} // unicli

#endif

