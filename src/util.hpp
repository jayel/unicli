#ifndef UNICLI_UTIL_HPP
#define UNICLI_UTIL_HPP

#include <iterator>
#include <unicode/unistr.h>
#include <functional>

namespace std {

// std::hash specialization for UnicodeString so that it can be used in
// unordered containers.
template <>
struct hash<icu::UnicodeString>
{
    size_t operator()(const icu::UnicodeString& s) const
    {
        return std::hash<int>()(s.hashCode());
    }
};

} // std

namespace unicli {

template <typename T>
struct irange : public std::iterator<std::input_iterator_tag, T>
{
    T m_value;

    irange(T x) : m_value(x) {}


    T& operator*() {
        return m_value;
    }

    irange& operator++() {
        ++m_value;
        return *this;
    }

    bool operator==(const irange& rhs) {
        return m_value == rhs.m_value;
    }

    bool operator!=(const irange& rhs) {
        return m_value != rhs.m_value;
    }
};

}

#endif

