#include "icu_backend.hpp"
#include "constants.hpp"
#include "util.hpp"
#include <algorithm>
#include <array>
#include <boost/lexical_cast.hpp>
#include <cassert>
#include <unicode/normalizer2.h>
#include <unicode/schriter.h>
#include <unicode/uchar.h>
#include <unicode/utf.h>


static std::array<const char*, 3> normalization_type = { {
    "nfc",
    "nfkc",
    "nfkc_cf"
} };

static std::array<const char*, 2> normalization_name = { {
    "Composition",
    "Decomposition"
} };

static std::array<UNormalization2Mode, 2> normalization_mode = { {
    UNORM2_COMPOSE,
    UNORM2_DECOMPOSE
} };


static std::string char_name(UChar32 cp)
{
    static const int BUF_SIZE = 256;
    char buf[BUF_SIZE];
    UErrorCode error = U_ZERO_ERROR;
    std::string name("<error>");

    u_charName(cp, U_UNICODE_CHAR_NAME, buf, BUF_SIZE-1, &error);
    if (U_SUCCESS(error))
        name = buf;
    return name;
}

// Returns a string with an hexadecimal code point encoded in UTF-8.
static std::string utf8(UChar32 cp)
{
    char raw[4];
    int len = 0;
    U8_APPEND_UNSAFE(raw, len, cp);
    std::ostringstream result;
    result << std::hex << static_cast<int>(0xff & raw[0]);
    for (int i = 1; i < len; ++i)
        result << ' ' << std::hex << static_cast<int>(0xff & raw[i]);
    return result.str();
}

// Returns a string with an hexadecimal code point encoded in UTF-16.
static std::string utf16(UChar32 cp)
{
    UChar raw[2];
    int len = 0;
    U16_APPEND_UNSAFE(raw, len, cp);
    std::ostringstream result;
    result << std::hex << raw[0];
    for (int i = 1; i < len; ++i)
        result << ' ' << std::hex << raw[i];
    return result.str();
}

// Returns a string with an hexadecimal code point.
static std::string utf32(UChar32 cp)
{
    std::ostringstream result;
    result << std::hex << cp;
    return result.str();
}

static std::string glyph(UChar32 cp)
{
    std::string result(U8_LENGTH(cp), 0);
    int i = 0;
    U8_APPEND_UNSAFE(&result.front(), i, cp);
    return result;
}

static const char* block_name(UChar32 cp)
{
    auto block = ublock_getCode(cp);
    return u_getPropertyValueName(UCHAR_BLOCK, block, U_SHORT_PROPERTY_NAME);
}

static void basic_properties(UChar32 cp, std::vector<unicli::property_t>& dest)
{
    dest.emplace_back(PROP_BASIC_START, char_name(cp));
    dest.emplace_back(PROP_BASIC_START + 1, glyph(cp));
    dest.emplace_back(PROP_BASIC_START + 2, utf32(cp));
    dest.emplace_back(PROP_BASIC_START + 3, utf16(cp));
    dest.emplace_back(PROP_BASIC_START + 4, utf8(cp));
    dest.emplace_back(PROP_BASIC_START + 5, block_name(cp));
}

static void binary_properties(UChar32 cp, std::vector<unicli::property_t>& dest)
{
    for (int p = PROP_UNICODE_START; p < PROP_UNICODE_END; ++p)
        dest.emplace_back(p, u_hasBinaryProperty(cp, (UProperty)p));
}


namespace unicli {

property_t::property_t(int t, int val) noexcept :
    m_type(t),
    m_value(boost::lexical_cast<std::string>(val))
{
}

const char* property_t::name(bool short_format) const noexcept
{
    if (short_format)
        return short_properties[m_type];
    return long_properties[m_type];
}

bool property_t::is_separator() const noexcept
{
    return m_type == PROP_SEPARATOR;
}

std::vector<property_t> properties(const icu::UnicodeString& s, int flags)
{
    int sz = 1; // for the separator
    if (GET_PROP(GROUP_BASIC, flags))
        sz += PROP_BASIC_END - PROP_BASIC_START;
    if (GET_PROP(GROUP_BINARY, flags))
        sz += PROP_UNICODE_END - PROP_UNICODE_START;
    std::vector<property_t> result;
    result.reserve(sz * s.countChar32());
    icu::StringCharacterIterator i(s);
    UChar32 cp = 0;
    for (; i.hasNext(); ) {
        cp = i.next32PostInc();
        if (GET_PROP(GROUP_BASIC, flags))
            basic_properties(cp, result);
        if (GET_PROP(GROUP_BINARY, flags))
            binary_properties(cp, result);
        result.emplace_back(PROP_SEPARATOR, "");
    }
    return result;
}

std::vector<normalization_t> normalize(const icu::UnicodeString& input)
{
    std::vector<normalization_t> result;

    for (const auto& i : normalization_type) {
        for (const auto& j : normalization_mode) {
            UErrorCode err = U_ZERO_ERROR;
            auto normalizer = icu::Normalizer2::getInstance(0, i, j, err);
            icu::UnicodeString dest;
            normalizer->normalize(input, dest, err);
            if (U_FAILURE(err))
                continue;
            std::string tmp;
            dest.toUTF8String(tmp);
            result.emplace_back(i, normalization_name[j], std::move(tmp));
        }
    }
    return result;
}

std::vector<token_t> tokenize(const icu::UnicodeString& input)
{
    std::vector<token_t> result;
    result.reserve(input.countChar32());
    icu::StringCharacterIterator i(input);
    UChar32 cp = 0;

    while (i.hasNext()) {
        cp = i.next32PostInc();
        result.emplace_back(glyph(cp), utf32(cp));
    }
    return result;
}

} // unicli

