#include "name_map.hpp"
#include <iostream>
#include <algorithm>
#include <unicode/schriter.h>
#include <unicode/uchar.h>
#include <unicode/uniset.h>
#include <unicode/utf.h>
#include <unicode/ustream.h>
#include <unicode/usetiter.h>
#include <unicode/uniset.h>

#define BUFSIZE 128
#define MAPSIZE 100000

namespace unicli {

std::vector<icu::UnicodeString> tokenize_name(const icu::UnicodeString& input)
{
    icu::UnicodeString tmp;
    std::vector<icu::UnicodeString> result;
    icu::StringCharacterIterator i(input);
    UChar32 c = i.first32();
    bool capturing = false;
    while (c != icu::CharacterIterator::DONE) {
        if (u_isspace(c)) {
            if (capturing) {
                capturing = false;
                result.push_back(tmp);
                tmp.remove();
            }
        }
        else {
            if (!capturing)
                capturing = true;
            tmp += c;
        }
        c = i.next32();
    }
    if (capturing && tmp.length())
        result.push_back(tmp);
    return result;
}

name_map create_name_map()
{
    name_map data;
    uint32_t len = 0;
    UErrorCode error = U_ZERO_ERROR;
    std::vector<char> buffer(BUFSIZE);
    data.reserve(MAPSIZE);
    uint32_t maxlen = 0;
    for (UChar32 i = UCHAR_MIN_VALUE; i <= UCHAR_MAX_VALUE; ++i) {
        len = 0;
        do {
            if (len >= buffer.size())
                buffer.resize(len);
            error = U_ZERO_ERROR;
            len = u_charName(i, U_UNICODE_CHAR_NAME, buffer.data(), buffer.size(), &error);
            maxlen = std::max(maxlen, len);
        } while (len >= buffer.size());
        if (!len)
            continue;
        if (U_FAILURE(error)) {
            std::cerr << u_errorName(error) << "\n";
            continue;
        }
        auto tokens = tokenize_name(buffer.data());
        std::for_each(tokens.begin(), tokens.end(),
            [&, i](decltype(tokens.front()) t) { data[t].insert(i); });
    }
    return data;
}

} // unicli

