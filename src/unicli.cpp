#include "command.hpp"
#include "program_state.hpp"
#include <array>
#include <string>
#include <iostream>
#include <fstream>
#include <readline/history.h>
#include <readline/readline.h>
#include <unicode/uchar.h>
#include <unicode/uclean.h>
#include <boost/algorithm/string/trim.hpp>

#define STR1(x) #x
#define STR(x) STR1(x)
#define UNICLI_VERSION_MAJOR 1
#define UNICLI_VERSION_MINOR 0
#define UNICLI_VERSION_PATCH 1
#define UNICLI_VERSION STR(UNICLI_VERSION_MAJOR) "." STR(UNICLI_VERSION_MINOR) "." STR(UNICLI_VERSION_PATCH)

namespace boa = boost::algorithm;

static unicli::command_t parse(const std::string& s)
{
    auto start = s.find_first_not_of(' ');
    auto input = s.substr(start, s.find_first_of(' ', start));
    return unicli::command_t::create(input, s);
}

static char* readline_generator(const char* text, int current_state)
{
    static size_t list_index = 0;
    static int len = 0;
    const char* name = nullptr;

    if (!current_state) {
        list_index = 0;
        len = strlen(text);
    }
    while (list_index < unicli::command_name.size() &&
            (name = unicli::command_name[list_index++])) {
        if (!strncmp(name, text, len)) {
            char* tmp = (char*)malloc(strlen(name) + 1);
            strcpy(tmp, name);
            return tmp;
        }
    }
    return nullptr;
}

static char** readline_completion(const char *text, int start, int)
{
    char** matches = nullptr;
    if (!start)
        matches = rl_completion_matches(text, &readline_generator);
    else
        rl_bind_key('\t', rl_abort);
    return matches;
}

int main(int, char**)
{
    std::cout << "Program version: " << UNICLI_VERSION << '\n';
    std::cout << "Unicode version: " << U_UNICODE_VERSION << '\n';
    auto state = unicli::load_config(CONFIG_FILE);
    if (state.prompt.back() != ' ')
        state.prompt += ' ';
    rl_attempted_completion_function = readline_completion;
    while (!state.done) {
        rl_bind_key('\t', rl_complete);
        char* raw = readline(state.prompt.c_str());
        if (raw == nullptr || raw[0] == 0)
            continue;
        std::string input(raw);
        boa::trim(input);
        if (input.length()) {
            auto command = parse(input);
            command.run(state);
            add_history(raw);
        }
        free(raw);
    }
    save_config(state);
    u_cleanup();
    return 0;
}

