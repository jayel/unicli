# unicli

*unicli* is a command line application to quickly view character properties for
Unicode code points.

## Compiling

You need the following tools and libraries to be able to compile it:

- cmake >= 2.8
- g++ >= 4.4
- icu >= 4.9
- readline

This application has only been tested on GNU/Linux and it may or may not work on other operating systems.

Once you have all packages and repositories:

    cd unicli/
    cmake .
    make

## Tutorial

When starting the application, it will show a prompt waiting for the user to enter any text.
After writing something and pressing enter the program will do something depending on which mode is activated.
*unicli* has different types of input and output modes, depending on how you
want to select code points and how you want to see the information about them.

### Input modes:
1. Literal (cli: literal):
    Write or paste any character like 'a', '東', etc. The program expects the
    input to be UTF-8, make sure your terminal is correctly set.
2. Hexadecimal (cli: hex):
    Write a full code point in hexadecimal, for example '20ac', or a range of
    code points, for example 'a1-bc'.
3. Block (cli: block):
    Get all the code points from a specific block, for example 'ascii'.
4. Search (cli: search):
    Search for code points matching the specified name, for example ('latin a').

### Output modes:
1. Info (cli: info):
    Displays detailed data about a code point in order, one code point after
    another.
2. Comparison (cli: compare):
    Displays code point data in a table format that makes it easy to compare
    properties.
3. Normalization (cli: normalize):
    Normalizes the input supplied to all normalization forms for easy
    comparison.
4. Tokenization (cli: tokenize):
    Displays the individual code points existing in the input supplied in a
    compact manner.

This program uses readline, and supports command completion and command history.
